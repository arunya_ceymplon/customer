<?php

namespace App\Observers;

use App\Customer;

class CustomerObserver
{

    public function creating (Customer $customer )
    {

        $customer->user_id = auth()->id();

    }


    public function updating(Customer $customer)
    {
        if($customer->user_id == auth()->id())
            return true;
        else
            return false;

    }


    public function deleting(Customer $customer)
    {
        if($customer->user_id == auth()->id())
            return true;
        else
            return false;
    }


    public function restored(Customer $customer)
    {
        //
    }


    public function forceDeleted(Customer $customer)
    {
        //
    }
}
